;
; Tail call elimination detailed here.
; Assemble this with Jasmin: github.com/davidar/jasmin
;

.class public Factorial
.super java/lang/Object

.method public <init>()V
   aload_0
   invokespecial java/lang/Object/<init>()V
   return
.end method

.method public static main([Ljava/lang/String;)V

  .limit stack 4
  .limit locals 1

  ; pseudo: println(factorial(5, 1))

  getstatic java/lang/System.out Ljava/io/PrintStream;

  iconst_5
  lconst_1
  invokestatic Factorial/factorialIterative(IJ)J

  invokevirtual java/io/PrintStream/println(J)V
  return

.end method

.method public static factorialRecursive(IJ)J

  .limit stack 5
  .limit locals 3

  iload_0
  ifne recurse
  lload_1
  lreturn

recurse:
  iload_0
  iconst_1
  isub
  iload_0
  i2l
  lload_1
  lmul
  invokestatic Factorial/factorialRecursive(IJ)J
  lreturn

.end method

.method public static factorialIterative(IJ)J

  .limit stack 5
  .limit locals 3

start:
  iload_0
  ifne recurse
  lload_1
  lreturn

recurse:
  iload_0
  iconst_1
  isub
  iload_0
  i2l
  lload_1
  lmul

;
; Begin injected code
;
    lstore_1
    istore_0
    goto start
;
; End injected code
;
; Removed : invokestatic Factorial/factorialIterative(IJ)J
;         : lreturn
;

.end method
