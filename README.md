# Tail Call Agent
Ad hoc tail call elimination for Java and other JVM langs.

## Example / How-to

Lists are nice structures that we can recurse through.

```
class List {
  int value;
  List tail;

  boolean contains(int x) {
    return List.contains(this, x);
  }

  private static boolean contains(List xs, int x) {
    if (xs == null) {
      return false;
    }

    if (xs.value == x) {
      return true;
    }

    return contains(xs.tail, x);
  }
}
```

Assume we've also got `List.range(start, end)`.

```
List xs = List.range(1, 5); // [1, 2, 3, 4, 5]
print(xs.contains(3)); // "true"
```

What happens when we have a super long list?

```
List ys = List.range(1, 1_000_000); // [1, 2, ..., 1_000_000]
print(ys.contains(3)); // "true"
```

Still works, right? What about...

```
print("500k is in our list? " + ys.contains(500_000));
```

Let's run it and see.

```java_holder_method_tree
$ java SuperLongListStuff

Exception in thread "main" java.lang.StackOverflowError
    at List.contains(List.java:42)
    at List.contains(List.java:42)
    at List.contains(List.java:42)
    ...
```

Yikes, we've recursed too deep!

Since `contains` is tail-recursive, we can pass it over to Tail Call Agent.

```
$ java -javaagent:TailCallAgent.jar SuperLongListStuff

500k is in our list? true
```

Easy, right? Head over to [releases](https://github.com/evanram/tail-call-agent/releases) and try it yourself.
