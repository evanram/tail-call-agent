package coffee.evan.tca;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ClassFilterTest {
    @Test
    public void defaultFilterRejectsStdlibClasses() {
        ClassFilter filter = ClassFilter.getDefaultFilter();

        assertFalse(filter.includes("java.foo.Bar"));
        assertFalse(filter.includes("java.lang.Something"));
        assertFalse(filter.includes("javax.Something"));
        assertFalse(filter.includes("com.sun.Something"));
        assertFalse(filter.includes("sun.misc.Unsafe"));

        assertTrue(filter.includes("ClassInDefaultPkg"));
        assertTrue(filter.includes("com.foo.Bar"));
        assertTrue(filter.includes("foo.bar.Baz"));

        List<String> expected = new ArrayList<>();
        expected.add("+*");
        expected.add("-com.sun.*");
        expected.add("-java.*");
        expected.add("-javax.*");
        expected.add("-sun.*");

        assertLinesMatch(expected, filter.flatten());
    }

    @Test
    public void nullaryFactoryMethodUsesDefaultFilter() {
        ClassFilter filter = ClassFilter.of();
        assertEquals(filter, ClassFilter.getDefaultFilter());
    }

    @Test
    public void allowEverything() {
        ClassFilter filter = ClassFilter.of("*");

        assertTrue(filter.includes("ClassInDefaultPkg"));
        assertTrue(filter.includes("java.lang.Object"));

        List<String> expected = new ArrayList<>();
        expected.add("+*");

        assertLinesMatch(expected, filter.flatten());
    }

    @Test
    public void rejectEverything() {
        ClassFilter filter = ClassFilter.of("-*");

        assertFalse(filter.includes("ClassInDefaultPkg"));
        assertFalse(filter.includes("java.lang.Object"));

        List<String> expected = new ArrayList<>();
        expected.add("-*");

        assertLinesMatch(expected, filter.flatten());
    }

    @Test
    public void acceptAllButOneClass() {
        ClassFilter filter = ClassFilter.of("*", "-coffee.evan.Foo");

        assertFalse(filter.includes("coffee.evan.Foo"));
        assertTrue(filter.includes("coffee.evan.Bar"));

        assertTrue(filter.includes("ClassInDefaultPkg"));
        assertTrue(filter.includes("java.lang.Object"));

        List<String> expected = new ArrayList<>();
        expected.add("+*");
        expected.add("-coffee.evan.Foo");

        assertLinesMatch(expected, filter.flatten());
    }

    @Test
    public void rejectAllButOneClass() {
        ClassFilter filter = ClassFilter.of("-*", "coffee.evan.Foo");

        assertTrue(filter.includes("coffee.evan.Foo"));
        assertFalse(filter.includes("coffee.evan.Bar"));

        assertFalse(filter.includes("ClassInDefaultPkg"));
        assertFalse(filter.includes("java.lang.Object"));

        List<String> expected = new ArrayList<>();
        expected.add("+coffee.evan.Foo");
        expected.add("-*");

        assertLinesMatch(expected, filter.flatten());
    }

    @Test
    public void selectivelyIncludeInsideExcludedSubpath() {
        ClassFilter filter = ClassFilter.of("coffee.evan.*", "-coffee.evan.foo.*", "+coffee.evan.foo.Foo");

        assertFalse(filter.includes("coffee"));
        assertFalse(filter.includes("coffee.evan"));
        assertFalse(filter.includes("coffee.evan.foo.Xyz"));

        assertTrue(filter.includes("coffee.evan.Xyz"));
        assertTrue(filter.includes("coffee.evan.abc.Xyz"));
        assertTrue(filter.includes("coffee.evan.foo.Foo"));

        List<String> expected = new ArrayList<>();
        expected.add("+coffee.evan.*");
        expected.add("+coffee.evan.foo.Foo");
        expected.add("-coffee.evan.foo.*");

        assertLinesMatch(expected, filter.flatten());
    }
}
