package coffee.evan.tca.resources;

import java.math.BigInteger;

public class Factorial extends WrappedClass {
    public Factorial(Class<?> wrapped) {
        super("testclasspkgname.Factorial", wrapped);
    }

    public BigInteger factorialIterative(long n) {
        return invoke("factorialIterative", new Class<?>[] { long.class }, null, n);
    }

    public BigInteger factorialRecursive(long n) {
        return invoke("factorialRecursive", new Class<?>[] { long.class }, null, n);
    }
}
