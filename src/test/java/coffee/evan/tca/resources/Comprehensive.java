package coffee.evan.tca.resources;

public final class Comprehensive extends WrappedClass {
    public Comprehensive(Class<?> wrapped) {
        super("testclasspkgname.Comprehensive", wrapped);
    }

    public boolean isAccEven(int acc) {
        Class<?>[] params = {
                Object.class, boolean.class, byte.class, char.class,
                short.class, int.class, long.class, float.class, double.class,
                int[].class, int[][].class, int[][][].class,
                long[].class, long[][].class, long[][][].class,
                Object[].class, Object[][].class, Object[][][].class,
                int.class /* acc */
        };

        Object[] args = {
                null, false, (byte) 0, (char) 0,
                (short) 0, 0, 0, 0, 0,
                null, null, null,
                null, null, null,
                null, null, null,
                acc
        };

        return invoke("isAccEven", params, null, args);
    }

    public boolean isAccEvenNonStatic(int acc) {
        Class<?>[] params = {
                Object.class, boolean.class, byte.class, char.class,
                short.class, int.class, long.class, float.class, double.class,
                int[].class, int[][].class, int[][][].class,
                long[].class, long[][].class, long[][][].class,
                Object[].class, Object[][].class, Object[][][].class,
                int.class /* acc */
        };

        Object[] args = {
                null, false, (byte) 0, (char) 0,
                (short) 0, 0, 0, 0, 0,
                null, null, null,
                null, null, null,
                null, null, null,
                acc
        };

        return invoke("isAccEvenNonStatic", params, instantiate(), args);
    }
}
