package coffee.evan.tca.resources;

import java.lang.reflect.Method;

public abstract class WrappedClass {
    private final Class<?> wrapped;

    protected WrappedClass(String resourceClassName, Class<?> wrapped) {
        if (!wrapped.getName().equals(resourceClassName)) {
            throw new IllegalArgumentException("Cannot wrap " + wrapped + " to " + resourceClassName);
        }

        this.wrapped = wrapped;
    }

    @SuppressWarnings("unchecked") // caller assumes safety
    protected <T> T invoke(String name, Class<?>[] paramTypes, Object instance, Object... args) {
        try {
            return (T) wrapped.getMethod(name, paramTypes).invoke(instance, args);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }

    protected Object instantiate() {
        return instantiate(new Class<?>[] {});
    }

    protected Object instantiate(Class<?>[] consParamTypes, Object... args) {
        try {
            return wrapped.getConstructor(consParamTypes).newInstance(args);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException(e);
        }
    }
}
