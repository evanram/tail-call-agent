package coffee.evan.tca;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.objectweb.asm.Opcodes;

public class TypeDescriptorTest {
    @Test
    public void primitiveType() {
        TypeDescriptor desc = TypeDescriptor.of("J");

        assertEquals("LONG", desc.toString());
        assertEquals(2, desc.getOperandStackWidth());
        assertEquals(Opcodes.LSTORE, desc.getStoreOpcode());
        assertEquals("J", desc.getRaw());
    }

    @Test
    public void objectType() {
        TypeDescriptor desc = TypeDescriptor.of("Ljava/lang/Object;");

        assertEquals("OBJECT (Ljava/lang/Object;)", desc.toString());
        assertEquals(1, desc.getOperandStackWidth());
        assertEquals(Opcodes.ASTORE, desc.getStoreOpcode());
        assertEquals("Ljava/lang/Object;", desc.getRaw());
    }

    @Test
    public void arrayOfPrimitive() {
        TypeDescriptor desc = TypeDescriptor.of("[J");

        assertEquals("ARRAY ([J)", desc.toString());
        assertEquals(1, desc.getOperandStackWidth());
        assertEquals(Opcodes.ASTORE, desc.getStoreOpcode());
        assertEquals("[J", desc.getRaw());
    }

    @Test
    public void multiArrayOfPrimitive() {
        TypeDescriptor desc = TypeDescriptor.of("[[J");

        assertEquals("ARRAY ([[J)", desc.toString());
        assertEquals(1, desc.getOperandStackWidth());
        assertEquals(Opcodes.ASTORE, desc.getStoreOpcode());
        assertEquals("[[J", desc.getRaw());
    }

    @Test
    public void arrayOfObject() {
        TypeDescriptor desc = TypeDescriptor.of("[Ljava/lang/Object;");

        assertEquals("ARRAY ([Ljava/lang/Object;)", desc.toString());
        assertEquals(1, desc.getOperandStackWidth());
        assertEquals(Opcodes.ASTORE, desc.getStoreOpcode());
        assertEquals("[Ljava/lang/Object;", desc.getRaw());
    }

    @Test
    public void multiArrayOfObject() {
        TypeDescriptor desc = TypeDescriptor.of("[[Ljava/lang/Object;");

        assertEquals("ARRAY ([[Ljava/lang/Object;)", desc.toString());
        assertEquals(1, desc.getOperandStackWidth());
        assertEquals(Opcodes.ASTORE, desc.getStoreOpcode());
        assertEquals("[[Ljava/lang/Object;", desc.getRaw());
    }

    @Test
    public void primitivesUseSharedObjects() {
        assertSame(TypeDescriptor.of("Z"), TypeDescriptor.of("Z"));
        assertNotSame(TypeDescriptor.of("Z"), TypeDescriptor.of("I"));

        // things we're not sharing
        assertNotSame(TypeDescriptor.of("[I"), TypeDescriptor.of("[I"));
        assertNotSame(TypeDescriptor.of("LFoo;"), TypeDescriptor.of("LFoo;"));
    }

    @Test
    public void equalsAndHashCode() {
        assertEquals(TypeDescriptor.of("Z").hashCode(), TypeDescriptor.of("Z").hashCode());
        assertEquals(TypeDescriptor.of("Z"), TypeDescriptor.of("Z"));

        assertNotEquals(TypeDescriptor.of("Z"), TypeDescriptor.of("I"));

        assertEquals(TypeDescriptor.of("LFoo;").hashCode(), TypeDescriptor.of("LFoo;").hashCode());
        assertEquals(TypeDescriptor.of("LFoo;"), TypeDescriptor.of("LFoo;"));
    }
}
