package coffee.evan.tca.transform;

import coffee.evan.tca.resources.Comprehensive;
import coffee.evan.tca.resources.Factorial;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static coffee.evan.tca.transform.JavaResourceUtils.loadClass;
import static org.junit.jupiter.api.Assertions.*;

public class TailCallEliminatorTest {
    @Test
    public void tailRecursiveFactorialDoesNotStackOverflow() {
        final String className = "testclasspkgname.Factorial";
        Class<?> uninstrumentedClass = loadClass(className);
        Class<?> instrumentedClass = loadClass(className, new TailCallEliminator());

        Factorial notTransformed = new Factorial(uninstrumentedClass);
        Factorial transformed = new Factorial(instrumentedClass);

        BigInteger fact50 = notTransformed.factorialIterative(50);
        assertEquals(fact50, notTransformed.factorialRecursive(50));
        assertEquals(fact50, transformed.factorialRecursive(50));
        assertEquals(fact50, transformed.factorialIterative(50));

        BigInteger fact30_000 = notTransformed.factorialIterative(30_000);
        assertEquals(fact30_000, transformed.factorialRecursive(30_000));
        assertEquals(fact30_000, transformed.factorialIterative(30_000));

        // `notTransformed.factorialRecursive(30_000)` will cause stack overflow here.
        // TODO: load tests with fixed stack size to prevent different VM settings from screwing with this
    }

    @Test
    public void allPrimitivesAndArrayParametersAreHandled() {
        final String className = "testclasspkgname.Comprehensive";
        Class<?> uninstrumentedClass = loadClass(className);
        Class<?> instrumentedClass = loadClass(className, new TailCallEliminator());

        Comprehensive notTransformed = new Comprehensive(uninstrumentedClass);
        Comprehensive transformed = new Comprehensive(instrumentedClass);

        assertTrue(notTransformed.isAccEven(12));
        assertFalse(notTransformed.isAccEven(13));

        assertTrue(notTransformed.isAccEvenNonStatic(12));
        assertFalse(notTransformed.isAccEvenNonStatic(13));

        assertTrue(transformed.isAccEven(12));
        assertFalse(transformed.isAccEven(13));

        assertTrue(transformed.isAccEvenNonStatic(12));
        assertFalse(transformed.isAccEvenNonStatic(13));

        // calls that will stack overflow if not optimized:
        assertTrue(transformed.isAccEven(10_000));
        assertFalse(transformed.isAccEven(10_001));

        assertTrue(transformed.isAccEvenNonStatic(10_000));
        assertFalse(transformed.isAccEvenNonStatic(10_001));
    }
}
