package coffee.evan.tca.transform;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.util.Textifier;
import org.objectweb.asm.util.TraceClassVisitor;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.*;

public final class JavaResourceUtils {
    private JavaResourceUtils() {}

    public static byte[] compile(String className) {
        try {
            String pathifiedClassName = className.replace('.', File.separatorChar);
            JavaCompiler c = ToolProvider.getSystemJavaCompiler();

            String resourcePath = File.separator + pathifiedClassName + ".java";
            String sourceFile = System.class.getResource(resourcePath).getFile();
            String parent = sourceFile.substring(0, sourceFile.lastIndexOf(pathifiedClassName));

            if (c.run(null, null, null, sourceFile) != 0) {
                throw new RuntimeException("Compilation error with: " + className);
            }

            File classFile = new File(parent + File.separator + pathifiedClassName + ".class");

            byte[] classFileBytes = new byte[(int) classFile.length()];
            FileInputStream fis = new FileInputStream(classFile);

            if (fis.read(classFileBytes) != classFile.length()) {
                throw new RuntimeException("Cannot fully read compiled resource: " + classFile);
            }

            fis.close();

            if (!classFile.delete()) {
                throw new RuntimeException("Could not delete compiled resource: " + classFile);
            }

            return classFileBytes;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Class<?> loadClass(String className, byte[] bytes) {
        class ByteArrayClassLoader extends ClassLoader {
            protected Class<?> load() {
                return defineClass(className, bytes, 0, bytes.length);
            }
        }

        return new ByteArrayClassLoader().load();
    }

    public static Class<?> loadClass(String className) {
        return loadClass(className, compile(className));
    }

    public static Class<?> loadClass(String className, FilteredTransformer transformer) {
        ClassReader reader = new ClassReader(compile(className));
        ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        ClassNode cn = new ClassNode();

        reader.accept(cn, 0);
        transformer.apply(cn);
        cn.accept(writer);

        return loadClass(className, writer.toByteArray());
    }

    public static void disassemble(byte[] classfile, OutputStream output) {
        ClassNode classNode = new ClassNode();
        ClassReader reader = new ClassReader(classfile);
        reader.accept(classNode, 0);
        TraceClassVisitor tcv = new TraceClassVisitor(classNode, new Textifier(), new PrintWriter(output));
        classNode.accept(tcv);
    }
}
