package coffee.evan.tca;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MethodDescriptorTest {
    @Test
    public void parseMethodDescriptor() {
        String raw = "(Z[[Ljava/lang/String;)V";
        MethodDescriptor desc = new MethodDescriptor(raw);

        List<TypeDescriptor> params = desc.getParameters();

        List<TypeDescriptor> expectedParams = new ArrayList<>();
        expectedParams.add(TypeDescriptor.of("Z"));
        expectedParams.add(TypeDescriptor.of("[[Ljava/lang/String;"));

        assertEquals(expectedParams, params);
        assertEquals(TypeDescriptor.of("V"), desc.getReturnType());
    }

    @Test
    public void parseNullaryMethodDescriptor() {
        String raw = "()Ljava/lang/Object;";
        MethodDescriptor desc = new MethodDescriptor(raw);

        assertEquals(new ArrayList<>(), desc.getParameters());
        assertEquals(TypeDescriptor.of("Ljava/lang/Object;"), desc.getReturnType());
    }
}
