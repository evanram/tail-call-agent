package testclasspkgname;

import java.math.BigInteger;

public class Factorial {
    public static BigInteger factorialIterative(long n) {
        BigInteger acc = BigInteger.valueOf(n);

        while (--n > 0) {
            acc = acc.multiply(BigInteger.valueOf(n));
        }

        return acc;
    }

    public static BigInteger factorialRecursive(long n) {
        return factorialRecursive(n, BigInteger.ONE);
    }

    private static BigInteger factorialRecursive(long n, BigInteger acc) {
        if (n == 0) {
            return acc;
        }

        return factorialRecursive(n - 1, acc.multiply(BigInteger.valueOf(n)));
    }
}
