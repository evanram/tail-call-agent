package testclasspkgname;

public class Comprehensive {
    /**
     * Somewhat comprehensive test of the various parameter types one can expect to see in a Java method.
     *
     * Precondition: Acc is a non-negative integer.
     * Returns whether or not acc is even.
     */
    public static boolean isAccEven(Object o, boolean z, byte b, char c, short s, int i, long j, float f, double d,
                                    int[]    ia1, int[][]    ia2, int[][][]    ia3,
                                    long[]   la1, long[][]   la2, long[][][]   la3,
                                    Object[] oa1, Object[][] oa2, Object[][][] oa3,
                                    int acc)
    {
        if (acc < 0) {
            throw new IllegalArgumentException("acc < 0, must be non-negative");
        }

        if (acc == 0) {
            return true;
        }

        if (acc == 1) {
            return false;
        }

        Object  x_o = new Object();
        boolean x_z = false;
        byte    x_b = 123;
        char    x_c = 123;
        short   x_s = 123;
        int     x_i = 123;
        long    x_j = 123;
        float   x_f = 12.3f;
        double  x_d = 12.3d;

        int[]        x_ia1 = new int[1];
        int[][]      x_ia2 = new int[1][2];
        int[][][]    x_ia3 = new int[1][2][3];
        long[]       x_la1 = new long[1];
        long[][]     x_la2 = new long[1][2];
        long[][][]   x_la3 = new long[1][2][3];
        Object[]     x_oa1 = new Object[1];
        Object[][]   x_oa2 = new Object[1][2];
        Object[][][] x_oa3 = new Object[1][2][3];

        return isAccEven(
            x_o, x_z, x_b, x_c, x_s, x_i, x_j, x_f, x_d,
            x_ia1, x_ia2, x_ia3,
            x_la1, x_la2, x_la3,
            x_oa1, x_oa2, x_oa3,
            acc - 2
        );
    }

    public boolean isAccEvenNonStatic(Object o, boolean z, byte b, char c, short s, int i, long j, float f, double d,
                                      int[]    ia1, int[][]    ia2, int[][][]    ia3,
                                      long[]   la1, long[][]   la2, long[][][]   la3,
                                      Object[] oa1, Object[][] oa2, Object[][][] oa3,
                                      int acc)
    {
        if (acc < 0) {
            throw new IllegalArgumentException("acc < 0, must be non-negative");
        }

        if (acc == 0) {
            return true;
        }

        if (acc == 1) {
            return false;
        }

        Object  x_o = new Object();
        boolean x_z = false;
        byte    x_b = 123;
        char    x_c = 123;
        short   x_s = 123;
        int     x_i = 123;
        long    x_j = 123;
        float   x_f = 12.3f;
        double  x_d = 12.3d;

        int[]        x_ia1 = new int[1];
        int[][]      x_ia2 = new int[1][2];
        int[][][]    x_ia3 = new int[1][2][3];
        long[]       x_la1 = new long[1];
        long[][]     x_la2 = new long[1][2];
        long[][][]   x_la3 = new long[1][2][3];
        Object[]     x_oa1 = new Object[1];
        Object[][]   x_oa2 = new Object[1][2];
        Object[][][] x_oa3 = new Object[1][2][3];

        return isAccEvenNonStatic(
                x_o, x_z, x_b, x_c, x_s, x_i, x_j, x_f, x_d,
                x_ia1, x_ia2, x_ia3,
                x_la1, x_la2, x_la3,
                x_oa1, x_oa2, x_oa3,
                acc - 2
        );
    }
}
