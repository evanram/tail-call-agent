package coffee.evan.tca;

import java.util.HashMap;
import java.util.Map;

import static org.objectweb.asm.Opcodes.*;

public class TypeDescriptor {
    private static final Map<Character, TypeDescriptor> cache = new HashMap<>();
    private static final Object lock = new Object();
    
    private final Type type;
    private final String raw;

    private TypeDescriptor(Type type, String raw) {
        this.type = type;
        this.raw = raw;
    }

    public static TypeDescriptor of(String raw) {
        if (raw == null || raw.length() == 0) {
            throw new IllegalArgumentException("Bad descriptor");
        }
        
        char symbol = raw.charAt(0);
        Type type = Type.LOOKUP.get(symbol);

        if (type == null) {
            throw new IllegalArgumentException("Not a descriptor: " + raw);
        }
        
        if (type != Type.OBJECT && type != Type.ARRAY) {
            if (raw.length() > 1) {
                throw new IllegalArgumentException("Not a descriptor: " + raw);
            }
            
            TypeDescriptor cached = cache.get(symbol);
            if (cached == null) {
                synchronized (lock) {
                    if (cached == null) {
                        cached = new TypeDescriptor(type, raw);
                        cache.put(symbol, cached);
                    }
                }
            }
            
            return cached;
        }

        return new TypeDescriptor(type, raw);
    }

    public String getRaw() {
        return raw;
    }

    public int getStoreOpcode() {
        return type.storeOpcode;
    }

    public int getOperandStackWidth() {
        return type.operandStackWidth;
    }

    @Override
    public String toString() {
        String ret = type.toString();

        if (raw.length() > 1) {
            ret += " (" + raw + ")";
        }

        return ret;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        return raw.equals(((TypeDescriptor) o).raw);
    }

    @Override
    public int hashCode() {
        return raw.hashCode();
    }

    private enum Type {
        BOOLEAN  ('Z', ISTORE,  1),
        BYTE     ('B', ISTORE,  1),
        CHAR     ('C', ISTORE,  1),
        SHORT    ('S', ISTORE,  1),
        INT      ('I', ISTORE,  1),
        FLOAT    ('F', FSTORE,  1),

        DOUBLE   ('D', DSTORE,  2),
        LONG     ('J', LSTORE,  2),

        OBJECT   ('L', ASTORE,  1),
        ARRAY    ('[', ASTORE,  1),

        // Void is a valid return-type, but is never on the operand stack.
        VOID     ('V', -1,     -1);

        static final Map<Character, Type> LOOKUP = new HashMap<>();
        static {
            for (Type t : Type.values()) {
                LOOKUP.put(t.symbol, t);
            }
        }

        final char symbol;
        final int storeOpcode;
        final int operandStackWidth;

        Type(char symbol, int storeOpcode, int operandStackWidth) {
            this.symbol = symbol;
            this.storeOpcode = storeOpcode;
            this.operandStackWidth = operandStackWidth;
        }
    }
}
