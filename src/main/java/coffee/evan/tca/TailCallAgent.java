package coffee.evan.tca;

import coffee.evan.tca.transform.TailCallEliminator;

import java.lang.instrument.Instrumentation;

public final class TailCallAgent {
    private TailCallAgent() {}

    public static void premain(String args, Instrumentation inst) {
        ClassFilter filter = null;
        boolean verbose = false;

        if (args != null) {
            for (String arg : args.split(" ")) {
                if (arg.startsWith("filter=")) {
                    arg = arg.substring(7);
                    filter = ClassFilter.of(arg.split(","));
                } else if (arg.equals("verbose")) {
                    verbose = true;
                }
            }
        }

        if (filter == null) {
            filter = ClassFilter.getDefaultFilter();
        }

        if (verbose) {
            System.out.println("Filtering the following classes:");
            filter.flatten().forEach(s -> System.out.println("\t" + s));
        }

        // TODO: configurable transformer types
        inst.addTransformer(new TailCallEliminator(filter, verbose));
    }
}

