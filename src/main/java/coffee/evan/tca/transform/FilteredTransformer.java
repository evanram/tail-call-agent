package coffee.evan.tca.transform;

import coffee.evan.tca.ClassFilter;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

public abstract class FilteredTransformer implements ClassFileTransformer {
    private final ClassFilter filter;
    protected final boolean verbose;

    public FilteredTransformer() {
        this.filter = ClassFilter.getDefaultFilter();
        this.verbose = false;
    }

    public FilteredTransformer(ClassFilter filter, boolean verbose) {
        this.filter = filter;
        this.verbose = verbose;
    }

    protected boolean apply(ClassNode cn) {
        boolean modified = false;
        for (MethodNode mn : cn.methods) {
            boolean methodWasModified = apply(mn);
            if (verbose && methodWasModified) {
                System.out.println("Transformed " + cn.name + ":" + mn.name + mn.desc);
            }

            modified |= methodWasModified;
        }

        return modified;
    }

    protected abstract boolean apply(MethodNode mn);

    @Override
    public final byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain, byte[] classfileBuffer)
            throws IllegalClassFormatException
    {
        if (filter.includes(className.replace('/', '.'))) {
            if (verbose) {
                System.out.println("Scanning " + className);
            }

            ClassNode cn = new ClassNode();
            new ClassReader(classfileBuffer).accept(cn, 0);

            if (apply(cn)) {
                ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
                cn.accept(writer);
                return writer.toByteArray();
            }
        }

        return classfileBuffer;
    }
}
