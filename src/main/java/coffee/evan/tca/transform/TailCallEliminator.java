package coffee.evan.tca.transform;

import coffee.evan.tca.ClassFilter;
import coffee.evan.tca.MethodDescriptor;
import coffee.evan.tca.TypeDescriptor;
import org.objectweb.asm.Label;
import org.objectweb.asm.tree.*;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

import static org.objectweb.asm.Opcodes.*;

public final class TailCallEliminator extends FilteredTransformer {
    public TailCallEliminator() {
        super();
    }

    public TailCallEliminator(ClassFilter filter, boolean verbose) {
        super(filter, verbose);
    }

    @Override
    protected boolean apply(MethodNode mn) {
        boolean optimizedAtLeastOneTailCall = false;

        AbstractInsnNode[] instructions = mn.instructions.toArray();
        InsnList modified = new InsnList();
        Label methodStartLabel = new Label();

        modified.add(new LabelNode(methodStartLabel));

        for (int i = 1; i < instructions.length; i++) {
            boolean eliminatedTailCall = false;

            // Find method call followed by return instruction
            if (isReturnOpcode(instructions[i].getOpcode()) &&
                    instructions[i - 1] instanceof MethodInsnNode &&

                    // TODO: check method's owner instead. <init> fails because super call has same name
                    !(mn.name.equals("<init>") || mn.name.equals("<clinit>")))
            {
                MethodInsnNode methodInsnNode = (MethodInsnNode) instructions[i - 1];

                // Is this call recursive? If so, it's a tail-recursive call.
                if (methodInsnNode.name.equals(mn.name) && methodInsnNode.desc.equals(mn.desc)) {
                    eliminatedTailCall = true;
                    optimizedAtLeastOneTailCall = true;

                    for (AbstractInsnNode storeInstruction : getStoreInstructions(mn.desc, mn.access)) {
                        modified.add(storeInstruction);
                    }

                    modified.remove(instructions[i - 1]); // method invoke
                    modified.add(new JumpInsnNode(GOTO, new LabelNode(methodStartLabel)));
                }
            }

            if (!eliminatedTailCall) {
                modified.add(instructions[i]);
            }
        }

        mn.instructions = modified;

        return optimizedAtLeastOneTailCall;
    }

    private boolean isReturnOpcode(int op) {
        return op >= IRETURN && op <= RETURN;
    }

    private Deque<AbstractInsnNode> getStoreInstructions(String methodDesc, final int access) {
        Deque<AbstractInsnNode> instructions = new ArrayDeque<>();
        List<TypeDescriptor> parameterTypes = new MethodDescriptor(methodDesc).getParameters();
        boolean nonStaticMethod = (access & ACC_STATIC) == 0;
        int parameterIndex = nonStaticMethod ? 1 : 0;

        for (TypeDescriptor typeDesc : parameterTypes) {
            instructions.addFirst(new VarInsnNode(typeDesc.getStoreOpcode(), parameterIndex));
            parameterIndex += typeDesc.getOperandStackWidth();
        }

        if (nonStaticMethod) {
            instructions.addLast(new VarInsnNode(ASTORE, 0));
        }

        return instructions;
    }
}
