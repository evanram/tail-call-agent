package coffee.evan.tca;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class MethodDescriptor {
    private final List<TypeDescriptor> parameters;
    private final TypeDescriptor returnType;

    public MethodDescriptor(String desc) {
        parameters = Collections.unmodifiableList(parseParameterTypes(desc));
        returnType = parseReturnType(desc);
    }

    public List<TypeDescriptor> getParameters() {
        return parameters;
    }

    public TypeDescriptor getReturnType() {
        return returnType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MethodDescriptor that = (MethodDescriptor) o;

        return parameters.equals(that.parameters) &&
                returnType.equals(that.returnType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parameters, returnType);
    }

    private static List<TypeDescriptor> parseParameterTypes(String desc) {
        List<TypeDescriptor> types = new ArrayList<>();
        String params = desc.substring(1 /* skip: '(' */, desc.indexOf(')'));

        int start = 0;
        while (start < params.length()) {
            char symbol = params.charAt(start);
            int end = start;

            // is parameter an object type? (e.g. `Ljava/lang/Object;`)
            if (symbol == 'L') {
                // look-ahead to scan full object type
                while (params.charAt(end) != ';') {
                    end++;
                }
            }

            // ... or is it an array type? (e.g. `[I` or `[[Ljava/lang/Object;`)
            else if (symbol == '[') {
                // look-ahead until we reach array's type symbol
                while (params.charAt(end) == '[') {
                    end++;
                }

                // in case of object array, continue look-ahead
                if (params.charAt(end) == 'L') {
                    while (params.charAt(end) != ';') {
                        end++;
                    }
                }
            }

            String raw = params.substring(start, end + 1);
            types.add(TypeDescriptor.of(raw));

            start = end + 1;
        }

        return types;
    }

    private static TypeDescriptor parseReturnType(String desc) {
        String raw = desc.substring(desc.lastIndexOf(')') + 1);
        return TypeDescriptor.of(raw);
    }
}
