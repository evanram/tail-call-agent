package coffee.evan.tca;

import java.util.ArrayList;
import java.util.List;

public final class ClassFilter {
    private final Node root;

    private ClassFilter(Node root) {
        if (root == null) {
            throw new IllegalArgumentException("Filter must contain a root node");
        }

        this.root = root;
    }

    public static ClassFilter of(String... filter) {
        if (filter.length == 0) {
            return getDefaultFilter();
        }

        Node root = new Node(null, false, new ArrayList<>(filter.length));

        for (String s : filter) {
            boolean include = true;

            s = s.trim();
            if (s.charAt(0) == '-') {
                include = false;
                s = s.substring(1);
            } else if (s.charAt(0) == '+') {
                s = s.substring(1);
            }

            String[] nodeValues = s.split("\\.");

            Node head = new Node(nodeValues[0], nodeValues.length != 1 || include, new ArrayList<>(1));
            Node cur = head;

            for (int i = 1; i < nodeValues.length; i++) {
                if (nodeValues[i].equals("*")) {
                    if (i != nodeValues.length - 1) {
                        throw new IllegalArgumentException("Invalid filter pattern: " + filter[i]);
                    }

                    cur.children.add(Node.wildcard(include));
                    /* end of loop */
                } else {
                    boolean includeSubNode = include || (i != nodeValues.length - 1);
                    Node next = new Node(nodeValues[i], includeSubNode, new ArrayList<>(1));
                    cur.children.add(next);
                    cur = next;
                }
            }

            root.appendNodeList(head);
        }

        return new ClassFilter(root);
    }

    public static ClassFilter getDefaultFilter() {
        return DefaultFilterHolder.defaultFilter;
    }

    public boolean includes(String className) {
        if (className.equals("")) {
            return false;
        }

        String[] name = className.split("\\.");
        int level = 0;

        Node next = root;
        boolean hasInclusiveParentWildcard = false;

        while (true) {
            if (level == name.length) {
                return hasInclusiveParentWildcard;
            }

            Node cur = null;
            Node wildcard = null;

            for (Node child : next.children) {
                if (child.isWildcard()) {
                    wildcard = child;
                }

                if (child.value.equals(name[level])) {
                    cur = child;
                    break;
                }
            }

            if (wildcard != null && wildcard.included) {
                // If we later traverse down a dead-end, we want to keep track of if we have seen an inclusive
                // wildcard up to this point or not (to prevent having to back-track).
                hasInclusiveParentWildcard = true;
            }

            if (cur == null) {
                if (wildcard != null) {
                    return wildcard.included;
                }

                return hasInclusiveParentWildcard;
            } else if (cur.children.size() == 0) {
                return cur.included;
            }

            next = cur;
            level++;
        }
    }

    public List<String> flatten() {
        List<String> leaves = new ArrayList<>();
        root.children.forEach(child -> child.flatten(leaves, null));
        leaves.sort(String::compareTo);

        return leaves;
    }

    private static class DefaultFilterHolder {
        static final ClassFilter defaultFilter = of("*", "-java.*", "-javax.*", "-com.sun.*", "-sun.*");
    }

    private static class Node {
        static final Node INCLUDED_WILDCARD = new Node("*", true, new ArrayList<>(0));
        static final Node EXCLUDED_WILDCARD = new Node("*", false, new ArrayList<>(0));

        final String value;
        final boolean included;
        final List<Node> children;

        Node(String value, boolean included, List<Node> children) {
            this.value = value;
            this.included = included;
            this.children = children;
        }

        static Node wildcard(boolean included) {
            return included ? INCLUDED_WILDCARD : EXCLUDED_WILDCARD;
        }

        boolean isWildcard() {
            return value.equals("*");//this == INCLUDED_WILDCARD || this == EXCLUDED_WILDCARD;
        }

        void appendNodeList(Node listHead) {
            for (Node child : children) {
                if (child.value.equals(listHead.value)) {
                    Node next = listHead.children.get(0);
                    child.appendNodeList(next); // XXX notice the tail-call on same type as caller's object
                    return;
                }
            }

            this.children.add(listHead);
        }

        void flatten(List<String> leaves, String acc) {
            String nextAcc = (acc == null) ? value : (acc + "." + value);

            if (children.size() == 0) {
                leaves.add((included ? "+" : "-") + nextAcc);
            } else {
                children.forEach(child -> child.flatten(leaves, nextAcc));
            }
        }
    }
}
